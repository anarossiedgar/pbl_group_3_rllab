#!/usr/bin/env python

import rospy
from std_msgs.msg import String
from std_msgs.msg import Float64
from std_msgs.msg import Header
from control_msgs.msg import JointControllerState
from std_srvs.srv import Empty
from sensor_msgs.msg import JointState
from sensor_msgs.msg import CompressedImage
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Point
from robotis_controller_msgs.msg import JointCtrlModule
from op3_walking_module_msgs.msg import WalkingParam
from op3_ball_detector.msg import CircleSetStamped
from pbl_demonstration.srv import TargetDirection

import numpy as np
from moveit_commander import MoveGroupCommander, RobotCommander
from geometry_msgs.msg import Pose, PoseStamped

from numpy import round
import threading

class goalkeeper:
    def __init__(self):
        rospy.init_node('pbl_goalkeeper')

        self.enable_ctrl_module_pub  = rospy.Publisher('/robotis/enable_ctrl_module', String, queue_size=5)
        self.head_control_pub        = rospy.Publisher('/robotis/head_control/set_joint_states', JointState, queue_size=1)
        self.walk_command_pub        = rospy.Publisher('/robotis/walking/command', String, queue_size=5)
        self.walk_param_pub          = rospy.Publisher('/robotis/walking/set_params', WalkingParam, queue_size=5)

        # rospy.Subscriber('/ball_detector_node/image_out/compressed', CompressedImage, self.callback_image)
        rospy.Subscriber('/ball_detector_node/circle_set', CircleSetStamped, self.callback_circle)
        rospy.Subscriber('/robotis_op3/head_tilt_position/state', JointControllerState, self.callback_head_tilt_joint)

        self.circle_pos = CircleSetStamped()
        self.head_tilt_joint_pos = JointControllerState()
        self.walk_param = WalkingParam()
        self.head_param = JointState()
        rate = rospy.Rate(30)

        # Walking params
        ####### walking init pose #######
        self.walk_param.init_x_offset            = -0.02
        self.walk_param.init_y_offset            =  0.015
        self.walk_param.init_z_offset            =  0.035
        self.walk_param.init_roll_offset         =  0.0
        self.walk_param.init_pitch_offset        =  0.0
        self.walk_param.init_yaw_offset          =  0.0

        ####### time parameter #####
        self.walk_param.period_time              =  0.9
        self.walk_param.dsp_ratio                =  0.4
        self.walk_param.step_fb_ratio            =  0.28

        ########## walking parameter ########
        self.walk_param.x_move_amplitude         =  0.0
        self.walk_param.y_move_amplitude         =  0.0
        self.walk_param.z_move_amplitude         =  0.01
        self.walk_param.angle_move_amplitude     =  0.0
        self.walk_param.move_aim_on              =  False

        self.walk_speed                          =  0.04

        ########## balance parameter ##########
        self.walk_param.balance_enable           =  False
        self.walk_param.balance_hip_roll_gain    =  0.35
        self.walk_param.balance_knee_gain        =  0.3
        self.walk_param.balance_ankle_roll_gain  =  0.7
        self.walk_param.balance_ankle_pitch_gain =  0.9
        self.walk_param.y_swap_amplitude         =  0.02
        self.walk_param.z_swap_amplitude         =  0.005
        self.walk_param.arm_swing_gain           =  0.2
        self.walk_param.pelvis_offset            =  0.008
        self.walk_param.hip_pitch_offset         =  0.122

        ########## gain parameter ##########
        self.walk_param.p_gain                   =  0.0
        self.walk_param.i_gain                   =  0.0
        self.walk_param.d_gain                   =  0.0

        # Head params
        self.head_param.header                   =  Header()
        self.head_param.name                     =  ['head_tilt']
        self.head_param.position                 =  [round(self.head_tilt_joint_pos.set_point, 3)]
        self.head_param.velocity                 =  [0.0]
        self.head_param.effort                   =  [0.0]
        
        self.head_speed                          =  0.025

        #init for open manipulator
        self.group_arm = MoveGroupCommander("arm")
        self.group_arm.set_planning_time( 100.0 )
        self.group_arm.set_start_state_to_current_state()

    def callback_circle(self, data):
        self.circle_pos = data

    def callback_head_tilt_joint(self, data):
        self.head_tilt_joint_pos = data

    def unpause_ros(self):
        raw_input("Press Enter to Start")
        rospy.wait_for_service('/gazebo/unpause_physics')
        unpause_phys = rospy.ServiceProxy('/gazebo/unpause_physics', Empty)
        try:
            resp1 = unpause_phys()
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))

    def pause_ros(self):
        raw_input("Press Enter to Pause")
        rospy.wait_for_service('/gazebo/pause_physics')
        pause_phys = rospy.ServiceProxy('/gazebo/pause_physics', Empty)
        try:
            resp1 = pause_phys()
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))

    def check_position(self):
        rospy.wait_for_service('target_direction')
        try:
            target_direction_serv = rospy.ServiceProxy('target_direction', TargetDirection)
            resp = target_direction_serv(self.circle_pos)
            # print(resp)
            return(resp)
        except rospy.ServiceException as e:
            # print("Service call failed: %s"%e)
            pass

    def step(self, multiplier_x):
        self.walk_param.y_move_amplitude = multiplier_x * self.walk_speed
        self.walk_command = 'stop' if self.walk_param.y_move_amplitude == 0 else 'start'
        print(self.walk_param.y_move_amplitude, self.walk_command)
        self.walk_param_pub.publish(self.walk_param)
        rospy.sleep(0.1)
        self.walk_command_pub.publish(self.walk_command)
        rospy.sleep(1)

    def look(self, multiplier_y):
        cur_head_tilt_joint_state = round(self.head_tilt_joint_pos.set_point, 3)
        new_state = multiplier_y * self.head_speed + cur_head_tilt_joint_state
        self.head_param.position = [new_state]
        self.head_control_pub.publish(self.head_param)
        rospy.sleep(1)

    def op3_action(self):
        while not rospy.is_shutdown():
            logs = []
            next_direction = self.check_position()
            if next_direction is not None:
                
                # Set X direction
                if next_direction.direction[0] != "center":
                    if next_direction.direction[0] == "left":
                        multiplier_x = 1
                        logs.append("Going left")
                    else:
                        multiplier_x = -1
                        logs.append("Going right")
                else:
                    multiplier_x = 0
                    logs.append('X Centered')

                # Set Y direction
                if next_direction.direction[1] != "center":
                    if next_direction.direction[1] == "down":
                        multiplier_y = -1
                        logs.append("Looking down")
                    else:
                        multiplier_y = 1
                        logs.append("Looking up")
                else:
                    multiplier_y = 0
                    logs.append('Y Centered')
                print(logs)

                # Run action
                step_thread = threading.Thread(target=self.step, args=(multiplier_x,))
                look_thread = threading.Thread(target=self.look, args=(multiplier_y,))
                step_thread.start()
                look_thread.start()
                step_thread.join()
                look_thread.join()
                # self.step(multiplier_x)
                # self.look(multiplier_y)

    def open_manipulator_action(self):
        while not rospy.is_shutdown():
            # Set Open Manipulator action here
            print("KICK BALL")
            self.move_arm(0.08, 0.00, 0.0225, -0.00, 0.6957, 0.000, 0.71831)
            # Decide on a kicking action from three randomly selected actions.
            kick_pattern = np.random.randint(1,4)
            print("Kick pattern: {}" .format(kick_pattern))
            if kick_pattern == 1:#kick straight up
                self.move_arm(0.2058, 0.00, 0.02640, 0.0, 0.6956, 0.0, 0.71840)
            elif kick_pattern == 2:#Kick to the right.
                self.move_arm(0.192, -0.072, 0.02647, 0.13228, 0.68306, -0.13657, 0.70517)
            elif kick_pattern == 3:#Kick to the left
                self.move_arm(0.1918, 0.07237, 0.02644, -0.1322, 0.68309, 0.1365, 0.70514)
            else:
                print("wrong kick number")
            #Return to initial value
            self.move_arm(0.08, 0.00, 0.0225, -0.00, 0.6957, 0.000, 0.71831)
            break
    
    def move_arm(self, px, py, pz, ox, oy, oz, ow):
        self.pose_target = Pose()
        self.pose_target.position.x =  px
        self.pose_target.position.y =  py
        self.pose_target.position.z =  pz
        self.pose_target.orientation.x =  ox
        self.pose_target.orientation.y =  oy
        self.pose_target.orientation.z =  oz
        self.pose_target.orientation.w =  ow
        self.group_arm.set_joint_value_target( self.pose_target, True )
        self.group_arm.go()
        rospy.sleep(3.0)
        return

    def main(self):
        self.unpause_ros()
        self.enable_ctrl_module_pub.publish("walking_module")
        self.enable_ctrl_module_pub.publish("head_control_module")
        rospy.sleep(4)
        self.open_manipulator_action()
        self.op3_action()
        # self.pause_ros()

if __name__ == "__main__":
    goalkeeper = goalkeeper()
    try:
        goalkeeper.main()
    except rospy.ROSInterruptException:
        pass
